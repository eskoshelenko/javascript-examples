# Java Script

**Materials taken from:** 
> [How programming works](https://github.com/HowProgrammingWorks)

## Programming style
1. **[Async](./programming/asyncProgramming/)**
2. **[Meta](./programming/metaProgramming/)**
3. **[reactive](./programming/reactiveProgramming/)**

## Basics?
1. **[Functor](./basic/functor/)**
2. **[Generator](./basic/generator/)**
3. **[Generics](./basic/generics/)**
4. **[HttpRequest](./basic/httpRequest/)**
5. **[Isolation](./basic/isolation/)**
6. **[Iteration](./basic/iteration/)**
7. **[Mixin](./basic/mixin/)**
8. **[Pool](./basic/pool/)**
9. **[Proxy](./basic/proxy/)**
10. **[Reflect](./basic/reflect/)**
11. **[Reflection](./basic/reflection/)**
12. **[Reusable](./basic/reusable/)**

## Async
1. **[Adapters](./async/adapters/)**
2. **[AsyncAwait](./async/asyncAwait/)**
3. **[Callbacks](./async/callbacks/)**
4. **[Cancelable](./async/cancelable/)**
5. **[Collectors](./async/collectors/)**
6. **[Compose](./async/compose/)**
7. **[Deferred](./async/deferred/)**
8. **[Do](./async/do/)**
9. **[Future](./async/future/)**
10. **[Generator](./async/generator/)**
11. **[Iterator](./async/iterator/)**
12. **[Metasync](./async/metasync/)**
13. **[NonBlocking loops](./async/nonBlocking/)**
14. **[Promises](./async/promices/)**
15. **[Thenable](./async/thenable/)**

## Errors
1. **[Handling](./errors/handling/)**
2. **[Promise](./errors/promise/)**
3. **[Stack trace](./errors/stackTrace/)**

## Testing
1. **[Bench mark](./testing/benchMark/)**
2. **[Simple](./testing/testing/)**
3. **[Unit](./testing/unit/)**

## SOLID
1. **[Single responsibility](./solid/01.singleResponsibility/)**
2. **[Open closure](./solid/02.openClosed/)**
3. **[Liskov substitution](./solid/03.liskovSubstitution/)**
4. **[Inversion of control](./solid/04.inversionOfControl/)**
5. **[Dependency injections](./solid/05.dependencyInjection/)**

## GRASP

## Patterns
1. **[Anti patterns](./patterns/_antiPatterns/)**
2. **[Abstract factory](./patterns/abstractFactory/)**
3. **[Adapter](./patterns/adapter/)**
4. **[Association](./patterns/association/)**
5. **[Bridge](./patterns/bridge/)**
6. **[Builder](./patterns/builder/)**
7. **[Chaining](./patterns/chaining/)**
8. **[Chain of responsibility](./patterns/chainOfResponcibility/)**
9. **[Closure](./patterns/closure/)**
10. **[Command](./patterns/command/)**
11. **[Composite](./patterns/composite/)**
12. **[Creator](./patterns/creator/)**
13. **[Facade](./patterns/facade/)**
14. **[Factory](./patterns/factory/)**
15. **[Factory method](./patterns/factoryMethod/)**
16. **[Iterator](./patterns/iterator/)**
17. **[Mediator](./patterns/mediator/)**
18. **[Memoization](./patterns/memoization/)**
19. **[Observer](./patterns/observer/)**
20. **[Prototype](./patterns/prototype/)**
21. **[Pure Fabrication](./patterns/pureFabrication/)**
22. **[Recursion](./patterns/recursion/)**
23. **[Revealing constructor](./patterns/revealingConstructor/)**
24. **[Singleton](./patterns/singleton/)**
25. **[Strategy](./patterns/strategy/)**
26. **[Wrapper](./patterns/wrapper/)**

## Data structures
1. **[Arrays](./dataStructures/arrays/)**
2. **[Concurrent queue](./dataStructures/concurrentQueue/)**
3. **[Dequeue](./dataStructures/dequeue/)**
4. **[Linked list](./dataStructures/linkedList/)**
5. **[Map](./dataStructures/map/)**
6. **[Object](./dataStructures/objects/)**
7. **[Set](./dataStructures/set/)**
8. **[String](./dataStructures/string/)**
9. **[Trees](./dataStructures/trees/)**
10. **[Typed Arrays](./dataStructures/typedArrays/)**