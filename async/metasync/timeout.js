function timeout (msec, fn) {
  let timer = setTimeout(() => {
    if (timer) console.log('Function timed out')
    timer = null
  }, msec)
  return (...args) => {
    if (timer) {
      timer = null
      fn(...args)
    }
  }
}

const cancelable = fn => {
  const wrapper = (...args) => {
    if (fn) return fn(...args)
  }
  wrapper.cancel = () => {
    fn = null
  }
  return wrapper
}

const fn = par => {
  console.log('Function called, par: ' + par)
}

const fn100 = timeout(100, fn)
const fn200 = timeout(200, fn)
const f = cancelable(fn)

setTimeout(() => {
  fn100('first')
  fn200('second')
}, 150)

f('first')
f.cancel()
f('second')

const f1 = timeout(1000, fn)
const f2 = cancelable(fn)
const f3 = once(fn)
const f4 = limit(10, fn)
const f5 = throttle(10, 1000, fn)
const f6 = debounce(1000, fn)
const f7 = utils(fn)
  .limit(10)
  throttle(10, 100)
  .timeout(1000)