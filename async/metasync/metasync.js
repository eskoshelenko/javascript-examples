import metasync from 'metasync'
import fs from 'fs'

const ASYNC_TIMEOUT = 1000

// ======= Parallel, sequence flow ========

const f1 = f2 = f3 = f4 = f5 = f6 = f7 = f8 = f9 = () => {}

/**
 *                      +-> f4 --> ---+
 *                      +-> f5 --> ---+
 * --> f1 --> f2 --> f3 +-> f6 --> f7 +-> f9
 *                      +-> f8 --> ---+
 */

const fx = metasync.flow(
  [f1, f2, f3, [[f4, f5, [f6, f7], f8]], f9]
)

// ============ Data Collector ============

const dc1 = new metasync.DataCollector(4)
const dc2 = new metasync.DataCollector(4, 5000)


dc1.on('error', (err, key) => {})
dc2.on('timeout', (err, data) => {})
dc2.on('done', (err, data) => {})

dc1.collect(data)

const dc3 = metasync
  .collect(3)
  .timeout(5000)
  .done((err, data) => {})
dc3(item)

const dc4 = metasync
  .collect(['key1', 'key2', 'keyN'])
  .timeout(5000)
  .done((err, data) => {})
dc4(key, value)

const dc = metasync
  .collect(count)
  .distinct()
  .done((err, data) => {})

dc(key, error, value)
dc.pick(key, value)
dc.fail(key, error)
fs.resdFile(filename, dc.bind(null, key))
dc.take(key, fs.readFile, filename)

// ============ Key Collector =============

const kc = new metasync.KeyCollector(
  ['user', 'config', 'readme', 'timer'], data => console.log(data)
)

kc.collect('user', {name: 'Some Name'})

fs.readFile('HISTORY.md', (err, data) => kc.collect('history', data))
fs.readFile('README.md', (err, data) => kc.collect('readme', data))

setTimeout(
  () => kc.collect('timer', { date: new Date() }),
  ASYNC_TIMEOUT
)

// =============== Throttle ===============

const t1 = metasync.throttle(5000, f)

t1()
t1()
t1() // single call

setTimeout(t1, 7000) // another call
setTimeout(t1, 7000) // will be fired at about 7000 + 5000

// ================ Queue =================

const cq = metasync.queue(3)
  .wait(2000)
  .timeout(5000)
  .throttle(100, 1000)
  .process((item, cb) => cb(err, result))
  .success(item => {})
  .failure(item => {})
  .done(() => {})
  .drain(() => {})
