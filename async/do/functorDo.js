// npm install do

function chain(prev = null) {
  const cur = () => {
    if (cur.prev) {
      cur.prev.next = cur
      cur.prev()
    } else {
      cur.forward()
    }
  }
  cur.prev = prev
  cur.fn = null
  cur.args = null
  cur.do = (fn, ...args) => {
    cur.fn = fn
    cur.args = args
    return chain(cur)
  }
  cur.forward = () => {
    if (cur.fn) cur.fn(cur.args, () => {
      if (cur.next) cur.next.forward()
    })
  }
  return cur
}

const c1 = chain()
  .do(readConfig, 'myConfig')
  .do(doQuery, 'select * from cities')
  .do(httpGet, 'http://somehost.com')
  .do(readFile, 'README.md')

c1()